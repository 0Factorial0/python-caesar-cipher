def clear():
    import os
    #clear screen
    test_os = os.name
    if test_os == "nt":
        os.system('cls')
    elif test_os == "posix":
        os.system('clear')
    else:
        pass

def err():
    print("--------------------------------")
    print("Err.")
    print("--------------------------------")

def cipher():
    import os
    #clear the screen
    clear()
    #charset en-US
    chars = ["A","B","C","D","E","F","G","H","I","J","K","L","M","N","O","P","Q","R","S","T","U","V","W","X","Y","Z","a","b","c","d","e","f","g","h","i","j","k","l","m","n","o","p","q","r","s","t","u","v","w","x","y","z","1","2","3","4","5","6","7","8","9","0"," ",",",".",":","/","(",")","[","]","!","?","*","#","+","%","&","=","-"]
    userinput_ = []
    caesared = []
    print("--------------------------------")
    try:
        #get input
        userinput0 = input("Input Text To Cipher: ")
        #make it a list
        userinput_ = list(userinput0)
        #check the input
        for i in userinput_:
            if chars.__contains__(i):
                pass
            else:
                raise Exception()
    except:
        err()
    print("--------------------------------")
    try:
        #get input
        userinput1 = int(input("Input Cipher Size(1-10)(Negative For Decrypt): "))
        #check if 1-10
        if userinput1 > 10 or userinput1 == 0 or userinput1 < -10:
            err()
        #change the letters
        for i in userinput_:
            a = "null"
            #if decrypt
            if userinput1 < 0:
                try:
                    a = chars[(chars.index(i) + userinput1)]
                except:
                    a = chars[(len(chars) - userinput1)]
            #if encrypt
            elif userinput1 > 1:
                try:
                    a = chars[(chars.index(i) + userinput1)]
                except:
                    a = chars[(-1 + userinput1)]
            #add the letter
            caesared.append(a)
    except:
        err()
    #finishing character
    caesared.append("@")

    #make it a string
    final = ''.join(str(e) for e in caesared)

    #print output to file
    file_name = "cipher.py"
    file_path = os.path.dirname(os.path.abspath(file_name))
    file = open(file_path+"/output.txt","a")
    file.write(final+"\n")
    file.close()

    #print out
    print(final)

cipher()